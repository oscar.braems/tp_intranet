#!/bin/bash
cd "$(dirname "$0")"
declare -ar setupBDD=("glpi" "joomla" "limesurvey" "mantisbt" "mediawiki" "opencart" "phpBB" "phpmyfaq" "textpattern" "elgg" "cmsms")
declare -ar specifiqFunc=("phpmyadmin" "glpi" "elgg" "textpattern" "phpmyfaq")

setup_BDD()
{
	mysql -u root -e "CREATE DATABASE IF NOT EXISTS bdd_$1 CHARACTER SET utf8 COLLATE utf8_general_ci;"
  	mysql -u root -e "CREATE USER IF NOT EXISTS 'UTI_$1'@'%'  IDENTIFIED BY '$1123!';
		GRANT ALL PRIVILEGES ON bdd_$1.* TO 'UTI_$1'@'%'WITH GRANT OPTION;"
		mysql -u root -e "CREATE USER IF NOT EXISTS 'UTI_$1_role'@'%'  IDENTIFIED BY '$1123!';
    		GRANT ALL PRIVILEGES ON bdd_$1.* TO 'UTI_$1_role'@'%';"
}
phpmyadmin()
{
FILE=/var/www/html/phpmyadmin/config.inc.php
if ! [ -f "$FILE" ]; then
    cp /var/www/html/phpmyadmin/config.sample.inc.php /var/www/html/phpmyadmin/config.inc.php
fi
}
glpi(){
FILE=/etc/apache2/apache2.conf
if ! grep 'glpi' $FILE; then
   echo "<Directory /var/www/html/glpi>
        Options Indexes FollowSymLinks
        AllowOverride limit
        Require all granted
</Directory>" >> $FILE
fi

}
elgg(){
mkdir /home/Elgg/
mkdir /home/Elgg/data
chmod 777 /home/Elgg/data/
}
textpattern(){
  mysql -u root < PROC_textpattern.sql
}
phpmyfaq(){
  mysql -u root < PROC_phpmyfaq.sql
}
apt-get update -y
apt-get upgrade -y
apt install apache2 -y
apt install mariadb-server -y
apt-get install php7.4-mysqli php7.4 php7.4-mbstring php7.4-xml php7.4-gd php7.4-curl php7.4-zip php7.4-intl php7.4-ldap php7.4-apcu php7.4-xmlrpc php7.4-bz2 php7.4-imap php7.4-soap php7.4-dom -y
service apache2 restart

thefile1=$(ls -d /var/www/html/*/)
thefile2=($(ls -d *.zip))
for element in "${thefile2[@]}"
do
element=${element//.zip/""}
if [[ "$thefile1" == *"$element"* ]]; then
  echo "$element It's there!"
  else
  unzip "$element".zip -d /var/www/html
 fi
 chown -R www-data:www-data /var/www/html/$element
 if [[ " ${setupBDD[*]} " =~ "$element" ]]; then
  setup_BDD $element
fi
 if [[ " ${specifiqFunc[*]} " =~ "$element" ]]; then
  $element
fi
done
mv /var/www/html/index.html /var/www/html/index_old.html
cp -R monParfum /var/www/html/
cp index.php /var/www/html/
service apache2 restart