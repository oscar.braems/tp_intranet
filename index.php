
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="monParfum/css/bootstrap.css">
    <link rel="stylesheet" href="monParfum/css/monParfum.css">
    <?php
	require_once ('monParfum/modif_ip.php');
     ?>

</head>
<body>
<div id="entete"  class="container ">
    <div class="row align-items-center">
        <div class="col" style="max-width: 7em"><img class="logo" src="monParfum/image/logo.PNG" alt="logo - monParfum"> </div>
        <div id="titre" class="col"><h1>MonParfum</h1></div>
        <div class="col" style="max-width: 7em"></div>
        <div class="col" style="max-width: 7em"></div>
        <div class="col"></div>
        <div class="col" style="max-width: 11em"></div>
    </div>
    <div id="sous_titre" class="row">
        <div class="col">Lycée Saint Marguerite</div>
    </div>
</div><div class="container">
    <div id="navigation" class="row align-items-center">
        <div class="col"><a href="" >Accueil</a></div>
    </div>
    <div class="row" >
        <div class="col" style="height:2em;"></div>
    </div>


    <div id="corps" class="row">
        <table id="menu" >
            <thead>
            <tr>
                <th colspan="2"><h1 style="text-decoration: underline">Intranet</h1><br>
                </th>
            </tr>
            </thead>
            <tbody>
            <?php
            $dir    = './';
            $files2 = array_diff(scandir($dir), array('..', '.','monParfum','ficher compressee'));
            $i=1;
            foreach ($files2 as &$value) {
                if (is_dir($value)){
                    switch ($i) {
                        case 3:
                            $i = 1;
                        case 1:
                            ?>
                            <tr>
                            <td><a href="<?php echo $value; ?>" class="link-info"><?php echo $value; ?></a></td>
                            <?php
                            break;
                        case 2:
                            ?>
                            <td><a href="<?php echo $value; ?>" class="link-info"><?php echo $value; ?></a></td>
                            </tr>
                            <?php
                            break;
                    }
                    $i++;
                }
            }
            ?>
            </tbody>
        </table>
    </div>
</div>
</div>
</body>
<script src="js/bootstrap.js"></script>
</html>
