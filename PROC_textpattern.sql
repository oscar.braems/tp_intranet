USE `bdd_textpattern`;
DROP procedure IF EXISTS PROC_change_ip;

DELIMITER $$
  USE bdd_textpattern$$
  CREATE PROCEDURE PROC_change_ip(IN newIp VARCHAR(15))
  BEGIN
  UPDATE txp_prefs
  SET txp_prefs.val = CONCAT(newIp,"/textpattern")
  WHERE txp_prefs.name = 'siteurl';
  END$$
DELIMITER ;