USE `bdd_phpmyafaq`;
DROP procedure IF EXISTS PROC_change_ip;

DELIMITER $$
USE `bdd_phpmyfaq`;
CREATE PROCEDURE `PROC_change_ip`(IN `newIp` VARCHAR(15))
BEGIN
UPDATE faqconfig 
SET faqconfig.config_value = CONCAT("http://",newIp,"/phpmyfaq")
WHERE faqconfig.config_name = "main.referenceURL";
END$$
DELIMITER ;