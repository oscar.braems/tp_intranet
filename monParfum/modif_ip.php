<?php
require_once ('monParfum/fonctions_procedures.php');
if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {
    $url = "https";
} else {
    $url = "http";
}
$url .= "://";
$url .= $_SERVER['HTTP_HOST'];
$url .= $_SERVER['REQUEST_URI'];
$pos=strpos($url, '://', 0)+3;
$pos2=strpos($url, '/', $pos);
$newIp =substr($url, $pos, $pos2-$pos);
$dir    = './';
$files = array_diff(scandir($dir), array('..', '.','monParfum','ficher compressee'));
if(in_array('opencart',$files)) {
    change_ip_file_conf('opencart/config.php',[2,5],$newIp,"/");
    change_ip_file_conf('opencart/admin/config.php',[2,3,6,7],$newIp,"/");
}
if(in_array('mediawiki',$files)) {
    change_ip_file_conf('mediawiki/LocalSettings.php',[32],$newIp,'"');
}
if(in_array('elgg',$files)) {
    change_ip_file_conf('elgg/elgg-config/settings.php',[43],$newIp,"/");
}
if(in_array('textpattern',$files)) {
    change_ip_bdd('bdd_textpattern','textpattern123!','UTI_textpattern',$newIp);
}
if(in_array('phpmyfaq',$files)) {
    change_ip_bdd('bdd_phpmyfaq','phpmyfaq123!','UTI_phpmyfaq',$newIp);
}


