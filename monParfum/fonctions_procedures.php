
<?php

function change_ip_file_conf($file,$numLine,$newIp,$delemiteur){
    try {
// Ouvre un fichier pour lire un contenu existant
        $current = file($file);
        foreach ($numLine as &$value) {
            if (!empty($current[$value])) {
                $line = $current[$value];
                $pos = strpos($line, '://', 0) + 3;
                $pos2 = strpos($line, $delemiteur, $pos);
                $line = substr_replace($line, $newIp, $pos, $pos2 - $pos);
                $current[$value] = $line;
                // Écrit le résultat dans le fichier
                file_put_contents($file, $current);
            }
        }
    }catch (Throwable $e) {
    }
}
function change_ip_bdd($nomBDD,$mpd,$user,$newIp){
    try {
        $connexion = new PDO('mysql:host=localhost' . ';dbname=' . $nomBDD, $user, $mpd);
        $connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch (PDOException $e) {
        echo "Problème de : connexion Bdd " . $e->getMessage() . "<br/>";
    }
    try {
    $sql = 'CALL PROC_change_ip(?)';
    $stmt = $connexion->prepare($sql);

    $stmt->bindParam(1, $newIp, PDO::PARAM_STR, 15);

    $stmt->execute();
    } catch (PDOException $e) {

    }

}
